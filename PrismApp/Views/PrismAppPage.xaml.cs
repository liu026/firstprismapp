﻿using System.Diagnostics;
using Xamarin.Forms;

namespace PrismApp.Views
{
    public partial class PrismAppPage : ContentPage
    {
        public PrismAppPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}:  ctor");
            InitializeComponent();

        }
    }

}
