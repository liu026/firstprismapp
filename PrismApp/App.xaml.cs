﻿using System.Diagnostics;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using PrismApp.Views;
using PrismApp.ViewModels;

namespace PrismApp
{
    public partial class App : PrismApplication
    {
        //public App()
        //{
        //    InitializeComponent();

        //    MainPage = new PrismAppPage();
        //}

        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            NavigationService.NavigateAsync(nameof(PrismAppPage));
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnInitialized)}");

            containerRegistry.RegisterForNavigation<PrismAppPage, PrismAppPageViewModel>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
