﻿using System.Diagnostics;
using Prism.Mvvm;

namespace PrismApp.ViewModels
{
        public class PrismAppPageViewModel : BindableBase
        {
            public PrismAppPageViewModel()
            {
                Debug.WriteLine($"**** {this.GetType().Name}: ctor");
            }
        }
}